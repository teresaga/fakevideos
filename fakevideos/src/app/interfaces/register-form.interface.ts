export interface RegisterForm {
    name: string;
    surname: string;
    email: string;
    password: string;
    password2: string;
}