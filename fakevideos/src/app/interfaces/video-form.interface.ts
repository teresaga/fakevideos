export interface VideoForm {
  videoId: string;
  videoUrl: string;
  channelId: string;
  channelUrl: string;
  channelTitle: string;
  description: string;
  title: string;
  thumbnail: string;
}
