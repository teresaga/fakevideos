import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor ( private router: Router,
                private userService: UserService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {

      if (this.userService.estaLogueado()){
        console.log('PASO EL GUARD');
        return true;
      }else{
        console.log('Bloqueado por el guard');
        this.router.navigateByUrl('/login');
        return false;
      }
      console.log('pasa por el canActivate del guard');
    
  }
  
}
