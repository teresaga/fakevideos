import { Component, OnInit } from '@angular/core';
import { Video } from '../../models/video.model';
import { VideoService } from '../../services/video.service';

import Swal from 'sweetalert2';
@Component({
  selector: 'app-favoritevideos',
  templateUrl: './favoritevideos.component.html',
  styles: [],
})
export class FavoritevideosComponent implements OnInit {
  public videos: Video[] = [];

  public loading: boolean = true;

  constructor(private videoService: VideoService) {}

  ngOnInit(): void {
    this.loading = true;
    this.loadVideos();
  }

  loadVideos(){
    this.videoService.getFavoriteVideos()
                  .subscribe( videos => {
                    this.loading =  false;
                    this.videos = videos;
                    //console.log(videos);
                  });
  }

  deleteVideo( id ) {
    this.videoService.deleteVideo( id )
      .subscribe( (resp: any) => {
        if (resp.success == false){
          Swal.fire('Error', resp.message, 'error');
        }

        Swal.fire('Video eliminado','El video fue eliminado correctamente','success');
        this.loadVideos();
      });
  }
}
