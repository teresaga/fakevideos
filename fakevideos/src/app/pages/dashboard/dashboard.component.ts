import { Component, OnInit } from '@angular/core';
import { YoutubeService } from '../../services/youtube.service';
import { VideoService } from 'src/app/services/video.service';

import { VideoForm } from '../../interfaces/video-form.interface';

import Swal from 'sweetalert2';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styles: [],
})
export class DashboardComponent implements OnInit {
  
  loading = false;
  videos: VideoForm[];
  
  constructor( private youtubeService: YoutubeService,
                private videoService: VideoService ) {}

  ngOnInit(): void {
    this.search('');
  }

  search(termino: string) {
    this.loading = true;
    this.youtubeService.getVideos(termino)
      .subscribe((items: any) => {
        this.videos = items.map(item => {
          return {
            title: item.snippet.title?.slice(0,30),
            videoId: item.id.videoId,
            videoUrl: `https://www.youtube.com/watch?v=${item.id.videoId}`,
            channelId: item.snippet.channelId,
            channelUrl: `https://www.youtube.com/channel/${item.snippet.channelId}`,
            channelTitle: item.snippet.channelTitle,
            description: item.snippet.description?.slice(0,50),
            thumbnail: item.snippet.thumbnails.high.url
          };
        });
        this.loading = false;
      });
    }

    addVideo( video ) {
      this.videoService.addVideo( video )
        .subscribe( (resp: any) => {
          if (resp.success == false){
            Swal.fire('Error', resp.message, 'error');
          }

          Swal.fire('Video agregado','El video fue agregado correctamente','success');
        });
    }
}
