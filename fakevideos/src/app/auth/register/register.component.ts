import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: [ './register.component.css' ],
})
export class RegisterComponent {

  public formSubmitted = false;
  public captchaResponse = '';

  public registerForm = this.fb.group({
    name: ['', Validators.required ],
    surname: ['', Validators.required],
    email: ['', [Validators.required, Validators.email] ],
    password: ['', Validators.required ],
    password2: ['', Validators.required ],
  },{
    validators: this.equalPasswords('password', 'password2')
  });

  constructor( private fb: FormBuilder,
                private userService: UserService,
                private router: Router
    ) { }

  createUser() {
    this.formSubmitted = true;
    //console.log(this.registerForm.value);

    if (this.registerForm.invalid){
      return;
    }

    this.userService.createUser(this.registerForm.value)
              .subscribe( (resp: any) => {

                this.router.navigateByUrl('/');

                if (resp.success == false){
                  Swal.fire('Error', resp.message, 'error');
                }
              });
  }

  resolved(captchaResponse: string){
    this.captchaResponse = captchaResponse;
  }

  invalidField( field:string): boolean {
    if (this.registerForm.get(field).invalid && this.formSubmitted) {
      return true;
    } else {
      return false;
    }
  }

  invalidCaptcha( ): boolean {
    if ((this.captchaResponse == '') && this.formSubmitted) {
      return true;
    } else {
      return false;
    }
  }

  invalidPasswords( )  {
    const pwd1= this.registerForm.get('password').value;
    const pwd2= this.registerForm.get('password2').value;
    if ( (pwd1 !== pwd2) && this.formSubmitted) {
      return true;
    } else {
      return false;
    }
  }

  equalPasswords( pwd1Name, pwd2Name) {
      return (formGroup: FormGroup) => {

        const pwd1Control = formGroup.get(pwd1Name);
        const pwd2Control = formGroup.get(pwd2Name);
      
        if (pwd1Control.value === pwd2Control.value) {
          pwd2Control.setErrors(null)
        }else{
          pwd2Control.setErrors({ noEqual: true })
        }
      }
  }

}
