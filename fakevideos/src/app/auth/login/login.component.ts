import { Component } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.css' ],
})
export class LoginComponent {

  public formSubmitted = false;

  public loginForm = this.fb.group({
    email: ['', [Validators.required, Validators.email] ],
    password: ['', Validators.required ],
  });

  constructor( private router: Router, 
                private fb: FormBuilder ,
                private userService: UserService) { }

  login() {
    this.userService.login(this.loginForm.value)
                    .subscribe( (resp: any) => {
                      //console.log(resp);

                      if (resp.success == false) {
                        Swal.fire('Error', resp.message, 'error');
                      }

                      this.router.navigateByUrl('/');
                    }, (err) => {
                        console.log(err);
                        Swal.fire('Error', err, 'error');
                    });
    //this.router.navigateByUrl('/');
  }

}
