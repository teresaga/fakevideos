export class Video {

    constructor(
        public videoId: string,
        public videoUrl: string,
        public channelId: string,
        public channelUrl: string,
        public channelTitle: string,
        public description: string,
        public title: string,
        public thumbnail: string,
        public owner: string,
        public id?: string,
    ) {}
}