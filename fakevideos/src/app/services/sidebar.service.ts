import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  menu: any[] = [
    {
      title: 'Menú',
      icon: 'mdi mdi-gauge',
      submenu: [
        { title: "Lista de videos", url:''},
        { title: "Lista de Favoritos", url:'favorite' },
      ]
    }
  ];

  constructor() { }
}
