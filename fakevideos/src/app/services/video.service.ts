import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../../src/environments/environment';

import { FavoritesForm } from '../interfaces/favorites-form.interface';
import { Video } from '../models/video.model';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root',
})
export class VideoService {
  constructor(private http: HttpClient) {}

  get token(): string {
    return localStorage.getItem('token') || '';
  }

  get id(): string {
    return localStorage.getItem('id') || '';
  }

  get headers() {
    return {
      headers: {
        'x-token': this.token
      }
    }
  }

  getFavoriteVideos() {
    /* const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'x-token': this.token,
      }),
    }; */

    var result = this.http
      .get(`${base_url}/videos/${this.id}`, this.headers)
      .pipe(map((resp: any) => resp.data.videos));
    
    //  console.log('Does not throw error');
    
      return result;
  }

  addVideo( video: Video ) {
    video.owner = this.id;
    video.title = video.title.replace(/([\uE000-\uF8FF]|\uD83C[\uDF00-\uDFFF]|\uD83D[\uDC00-\uDDFF])/g, "")
    const url = `${ base_url }/video`;
    return this.http.post( url, video, this.headers );
  }

  deleteVideo( _id: string ) {
    
    const url = `${ base_url }/video/${ _id }`;
    return this.http.delete( url, this.headers );
  }
}
