import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

import { environment } from 'src/environments/environment';


import { RegisterForm } from '../interfaces/register-form.interface';
import { LoginForm } from '../interfaces/login-form.interface';
import { Router } from '@angular/router';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public token;

  constructor( private http: HttpClient,
                private router: Router) { }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('id');
    localStorage.removeItem('name');
    this.router.navigateByUrl('/login');
  }

  getToken() {
    let token = localStorage.getItem('token');

    if (token != "undefined"){
      this.token = token;
    }else{
      this.token = null;
    }
    return this.token;
  }

  estaLogueado() {
    var token = this.getToken();
    return (token) ? true : false;
  }

  createUser(formData: RegisterForm) {
    
    return this.http.post(`${ base_url }/users`, formData)
                    .pipe(
                      tap( (resp: any) => {
                        localStorage.setItem('id', resp.user.id)
                        localStorage.setItem('token', resp.token)
                        localStorage.setItem('name', resp.user.name +" " +resp.user.surname)
                      })
                    );
  }

  login(formData: LoginForm) {
    
    return this.http.post(`${ base_url }/login`, formData)
                    .pipe(
                      tap( (resp: any) => {

                        if(resp.success){
                          localStorage.setItem('id', resp.user.id)
                          localStorage.setItem('token', resp.token)
                          localStorage.setItem('name', resp.user.name +" "+ resp.user.surname)
                        }
                        
                      })
                    );
  
  }

}
