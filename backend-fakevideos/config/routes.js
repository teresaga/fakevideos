/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

    'GET /api/videos/:id': 'UserController.getvideos',
    'POST /api/users': 'UserController.create',
    
    'POST /api/video/': 'VideoController.create',
    'DELETE /api/video/:id': 'VideoController.delete',

    'POST /api/login': 'AuthController.login',
    
};
