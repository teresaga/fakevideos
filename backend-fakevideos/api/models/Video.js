/**
 * Video.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    videoId: {
      type: 'string',
    },

    videoUrl: {
      type: 'string',
    },

    channelId: {
      type: 'string',
    },

    channelUrl: {
      type: 'string',
    },

    channelTitle: {
      type: 'string',
    },

    title: {
      type: 'string',
    },

    description: {
      type: 'string',
    },

    thumbnail: {
      type: 'string',
    },

    owner: {
      model: 'user'
    }

  },

};

