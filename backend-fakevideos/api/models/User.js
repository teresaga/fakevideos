/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
const bcrypt = require("bcrypt-nodejs");
module.exports = {
  tableName: "users",

  attributes: {
    name: {
      type: "string",
      required: true,
    },
    surname: {
      type: "string",
      required: true,
    },
    email: {
      type: "string",
      isEmail: true,
      required: true,
      unique: true,
    },
    password: {
      type: "string",
      required: true,
    },
    videos: {
      collection: "video",
      via: "owner",
    },
  },
  customToJSON: function () {
    return _.omit(this, ["password"]);
  },
  beforeCreate: function (user, cb) {
    bcrypt.genSalt(10, function (err, salt) {
      bcrypt.hash(user.password, salt, null, function (err, hash) {
        if (err) return cb(err);
        user.password = hash;
        return cb();
      });
    });
  },
};
