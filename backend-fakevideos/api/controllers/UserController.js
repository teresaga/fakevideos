/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    getvideos: function(req, res){
        User.findOne({ where: { id: req.param('id') } })
            .populate('videos')
            .then(function(users){
                if(!users || users.length == 0){
                    return res.send({
                        'success': false,
                        'message': 'No hay registros',
                    })
                }

                return res.send({
                    'success': true,
                    'message': 'Busqueda realizada con exito',
                    'data': users,
                })
            })
            .catch(function(err){
                    sails.log.debug(err)

                    return res.send({
                        'success': false,
                        'message': 'Error al obtener usuarios',
                    })
                })
    },


    create: function(req, res){
        User.create(req.allParams()).fetch()
            .then(function(user){
                return res.send({
                    'success': true,
                    'message': 'Usuario creado con exito',
                    'user': user,
                    'token': jwToken.sign(user.id),
                });
            })
            .catch(function(err){
                sails.log.debug(err)
                return res.send({
                    'success': false,
                    'message': 'Error al crear usuario',
                });
            })
    }


};

