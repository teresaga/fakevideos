/**
 * VideoController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    create: function(req, res){
        Video.create(req.allParams())
            .then(function(video){
                return res.send({
                    'success': true,
                    'message': 'Video agregado con exito',
                    'video': video,
                })
            })
            .catch(function(err){
                sails.log.debug(err)
                return res.send({
                    'success': false,
                    'message': 'Error al agregar video',
                })
            })
    },
    delete: function(req, res){
        Video.destroy(req.param('id'))
                .then(function(video){
                    return res.send({
                        'success': true,
                        'message': 'El video se ha eliminado con exito'
                    })
                })
                .catch(function(err){
                    sails.log.debug(err)
                    return res.send({
                        'success': false,
                        'message': 'Error al eliminar video'
                    })
                })
    }
};

