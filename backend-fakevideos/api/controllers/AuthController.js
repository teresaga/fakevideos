/**
 * AuthController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const bcrypt = require("bcrypt-nodejs");

module.exports = {
  login: function (req, res) {
    User.findOne({ where: { email: req.allParams().email } })
      .then(function (user) {
        if (!user || user.length == 0) {
          return res.send({
            success: false,
            message: "Email no válido",
          });
        }

        const validPassword = bcrypt.compareSync(
          req.allParams().password,
          user.password
        );

        if (!validPassword) {
          return res.send({
            success: false,
            message: "Contraseña no válida",
          });
        }

        
        return res.json({
          success: true,
          user: user,
          token: jwToken.sign(user.id), //generate the token and send it in the response
        });
      })
      .catch(function (err) {
        sails.log.debug(err);

        return res.send({
          success: false,
          message: "Unable to fetch records",
        });
      });
  },
};
