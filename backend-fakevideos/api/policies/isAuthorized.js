module.exports = function (req, res, next) {
  const token = req.header("x-token");
  if (!token) {
    return res.status(401).json({
      ok: false,
      msg: "No hay token en la petición",
    });
  }
  jwToken.verify(token, function (err, decoded) {
    if (err) {
      return res.json(401, { err: "Invalid token" });
    }
    req.user = decoded;
    next();
  });
};
