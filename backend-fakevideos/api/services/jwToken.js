/**
 * Service to generate JWT
 */
var jwt = require('jsonwebtoken');

module.exports = {
	'sign': function(uid) {
		return jwt.sign({
			uid: uid
		}, sails.config.jwtSecret, {expiresIn: '1d'});
	},
	'verify': function(token, callback) {
		return jwt.verify(token, sails.config.jwtSecret, callback);
	}
};